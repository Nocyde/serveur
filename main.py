from fastapi import FastAPI
import random
from fastapi.middleware.cors import CORSMiddleware

import metier

app = FastAPI()

origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:3000",
    "http://impishfox.fr"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

global rooms

rooms = []

global deck

deck = {}


@app.post("/creer-salon")
async def create_room(host: metier.PersonPydantic): 
    """
    
    """    
    
    my_host = metier.Person(ID = 0, pseudo = host.pseudo)
    my_room = metier.Room(persons = [my_host])
    
    rooms.append(my_room)
    
    return {"ID" : my_room.ID}

@app.post("/rejoindre-salon")
async def join_room(person: metier.PersonPydantic, id_room: metier.RoomPydantic): 
    """
    
    """    
    
    #Retrouver la room où l'on ajoute un joueur
    my_room = None

    for room in rooms :
        if room.ID == id_room.ID :
            my_room = room
        
    if my_room == None :
        return False

    number_people = len(my_room.persons)

    if number_people > 7:
        return {"error" : "trop de gens déjà présents dans cette room !" }
    
    id_new_person = max(my_room.id_persons_used) +1
    
    new_person = metier.Person(id_new_person, person.pseudo)
    my_room.add_person(new_person)
    
    return {"id" : id_new_person}

@app.post("/creer-partie")
async def create_game(id_room: metier.RoomPydantic): 
    
    #Retrouver la room
    my_room = None
    

    for room in rooms :
        if room.ID == id_room.ID :
            my_room = room
        
    if my_room == None :
        return False

    #initier les rôles
    
    number_people = len(my_room.persons)

    generated_roles = ["gentil"]*number_people

    if number_people == 4:
        number_mechants = random.randint(1,2)
    elif number_people == 5:
        number_mechants = 2
    elif number_people == 6:
        number_mechants = 2
    elif number_people == 7:
        number_mechants = random.randint(2,3)
    elif number_people == 8:
        number_mechants = 3
    
    for i in range(number_mechants):
        generated_roles[i] = "méchant"
    
    random.shuffle(generated_roles)


    #initier les cartes

    distribution = {"vides" : None, "cables": None}

    if number_people == 4:
        distribution["vides"], distribution["cables"] = 15, 4
    elif number_people == 5:
        distribution["vides"], distribution["cables"] = 19, 5
    elif number_people == 6:
        distribution["vides"], distribution["cables"] = 23, 6
    elif number_people == 7:
        distribution["vides"], distribution["cables"] = 27, 7
    elif number_people == 8:
        distribution["vides"], distribution["cables"] = 31, 8

    generated_cards = ["vide"]*distribution["vides"] + ["cable"]*distribution["cables"] + ["boom"]
    random.shuffle(generated_cards)

    #créer le deck

    deck = []

    for i in range(len(generated_cards)):
        my_card = metier.Card(generated_cards[i],"verso")
        deck.append(my_card)
    
    i = 0
    j = 0

    first_to_play = random.randint(0,number_people-1)
        
    my_room.data_game = metier.DataGame(players = [], currentPlayer = first_to_play)
    for person in my_room.persons :
        
        player = metier.Player(person.ID, person.pseudo, generated_roles[i],ready = False, cards = deck[j:j+5])
        
        my_room.data_game.add_player(player)
        
        i+=1
        j+=5
    
    return True




@app.post("/get-data")
async def get_data(id_room: metier.RoomPydantic): 
    
    #Retrouver la room où l'on ajoute un joueur
    my_room = None
    

    for room in rooms :
        if room.ID == id_room.ID :
            my_room = room
        
    if my_room == None :
        return {'message':'erreur pour trouver la partie'}
    
    data_game = my_room.data_game
    if data_game == None :
        players = []
        for player in my_room.persons :
            players.append(player.pseudo)
        return {"game" : "non initiée", "players" : players}
    else :
        return data_game

@app.post("/ready")
async def ready(id_room: metier.RoomPydantic, id_player: metier.PlayerPydantic): 
    
    #Retrouver la room où l'on ajoute un joueur
    my_room = None
    

    for room in rooms :
        if room.ID == id_room.ID :
            my_room = room
        
    if my_room == None :
        return {"message":"partie non trouvée"}
    
    my_player = None
    
    for player in my_room.data_game.players :
        if player.ID == id_player.ID :
            my_player = player
        
    if my_player == None :
        return {"message":"joueur non trouvé"}
    
    my_player.ready = True
    
    types = []
    for card in my_player.cards:
        types.append(card.type)


    random.shuffle(types)

    shuffled_cards = []
    
    for i in range(len(types)):
        my_card = metier.Card(types[i],"verso")
        shuffled_cards.append(my_card)

    my_player.cards = shuffled_cards

    return True


@app.post("/flip-card")
async def flip_card(id_room: metier.RoomPydantic, id_player: metier.PlayerPydantic, num_card: metier.CardPydantic): 
    
    #Retrouver la room où l'on ajoute un joueur
    my_room = None    

    for room in rooms :
        if room.ID == id_room.ID :
            my_room = room
        
    if my_room == None :
        return {"message":"partie non trouvée"}
    
    my_player = None
    
    for player in my_room.data_game.players :
        if player.ID == id_player.ID :
            my_player = player
        
    if my_player == None :
        return {"message":"joueur non trouvé"}
    
    the_card = my_player.cards[num_card.number]

    
    list_mechants = []
    list_gentils = []
    for player in my_room.data_game.players :
        if player.role == "méchant":
            list_mechants.append(player.pseudo)
        else:
            list_gentils.append(player.pseudo)

    print("début")

    if the_card.type == "boom":
        print("boom")
        my_room.data_game.winnersList = list_mechants
    
    if the_card.type == "cable":
        print("cable")
        my_player.cards[num_card.number].face = "recto"
        my_room.data_game.discoveredCables += 1
        reste_cable = False
        for player in my_room.data_game.players :
            for card in player.cards :
                if card.face == "verso":
                    if card.type == "cable":
                        reste_cable = True
        if reste_cable == False:
            print("cable false")
            my_room.data_game.winnersList = list_gentils
    
    if my_room.data_game.currentTurn == my_room.data_game.numberOfPlayers:
        if my_room.data_game.currentRound == 4 and my_room.data_game.winnersList != list_gentils:
            print("round 4")
            my_room.data_game.winnersList = list_mechants
        else:
            print("turn 4/4")
            my_room.data_game.currentRound += 1
            my_room.data_game.currentTurn = 1
            my_room.data_game.currentPlayer = id_player.ID

            #on redistribue les cartes qui étaient encore verso
            cartes_restantes = []
            for player in my_room.data_game.players :
                for card in player.cards :
                    if card.face == "verso":
                        cartes_restantes.append(card.type)

            random.shuffle(cartes_restantes)

            deck = []

            for i in range(len(cartes_restantes)):
                my_card = metier.Card(cartes_restantes[i],"verso")
                deck.append(my_card)

            j = 0
            val = (6 - my_room.data_game.currentRound)
          

            for i in range(my_room.data_game.numberOfPlayers) :
                my_room.data_game.players[i].cards = deck[j:j+val]       
                my_room.data_game.players[i].ready = False
                j+=val

    else:
        my_room.data_game.currentTurn += 1
        print("+1")
        my_room.data_game.currentPlayer = id_player.ID
        my_player.cards[num_card.number].face = "recto"
 
    return {"message":"flip card a fonctionné jusqu'au bout :)"}

if __name__ == '__main__':
    app.run(debug=True)
