from pydantic import BaseModel

class RoomPydantic(BaseModel) :
    ID : int

class Room :
    last_id = 0
    
    def __init__(self, persons = [], data_game = False) :
        self.ID = Room.last_id + 1
        Room.update_id()
        
        self.persons = persons
        
        self.id_persons_used = []
        for person in persons :
            self.id_persons_used.append(person.ID)
            
        self.data_game = None
        
    def update_id() :
        Room.last_id += 1
        
    def add_person(self, person) :
        self.persons.append(person)
        self.id_persons_used.append(person.ID)
        
class PersonPydantic(BaseModel) :
    pseudo : str

class Person :
    ID_used = []
    
    def __init__(self, ID, pseudo) :
        self.ID = ID
        Person.ID_used.append(self.ID)
        self.pseudo = pseudo
    
class PlayerPydantic(BaseModel) :
    ID : int
    
class Player :
    
    def __init__(self, ID, pseudo, role = None, ready = False, cards = []) :
        self.ID = ID
        
        self.pseudo = pseudo
        self.role = role
        self.ready = ready
        self.cards = cards
        
class CardPydantic(BaseModel) :
    number : int

class Card :
    def __init__(self, type = "vide", face = "verso") :
        self.type = type
        self.face = face
        
class DataGame :
    def __init__(self, players = [], numberOfPlayers = 0, currentPlayer = 0, currentRound = 1, currentTurn = 1, discoveredCables = 0, winnersList = "vide") :
        self.players = players
        self.numberOfPlayers = numberOfPlayers
        self.currentPlayer = currentPlayer
        self.currentRound = currentRound
        self.currentTurn = currentTurn
        self.discoveredCables = discoveredCables
        self.winnersList = winnersList

    def add_player(self, player) :
        self.players.append(player)
        self.numberOfPlayers += 1
